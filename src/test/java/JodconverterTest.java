import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class JodconverterTest {

    @Test
    public void testHttpAvailable() throws IOException, InterruptedException {
        Thread.sleep(5*1000);
        String dockerIp = System.getProperty("eu.xenit.docker.private.ip");
        URL obj = new URL("http://" + dockerIp + ":9080/converter");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        int res = con.getResponseCode();
        assertEquals(200, res);
    }

}