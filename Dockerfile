FROM anapsix/alpine-java:7
MAINTAINER Wim <wim.evens@xenit.eu>
MAINTAINER Willem <willem.vandeneynde@xenit.eu>

RUN apk update

RUN apk add vim wget curl unzip libice libsm-dev fontconfig libxt libxfont cups-libs libxinerama libxrender libreoffice
RUN wget -O /tmp/open-sans.zip https://www.fontsquirrel.com/fonts/download/open-sansopen-sans.zip
RUN mkdir /root/.fonts \
  && unzip /tmp/open-sans.zip -d /root/.fonts \
  && rm -fr open-sans.zip
RUN fc-cache -fv

RUN wget -O jod-tomcat.zip 'http://sourceforge.net/projects/jodconverter/files/JODConverter/2.2.2/jodconverter-tomcat-2.2.2.zip/download'
RUN unzip jod-tomcat.zip -d /usr/local/src \
  && ln -s /usr/local/src/jodconverter-tomcat-2.2.2/bin/startup.sh /usr/bin/jod \
  && rm jod-tomcat.zip

ADD start /usr/bin/start-jod
ADD applicationContext.xml /usr/local/src/jodconverter-tomcat-2.2.2/webapps/converter/WEB-INF/applicationContext.xml

EXPOSE 8080
#HEALTHCHECK CMD curl -L --fail http://localhost:8080/converter/ || exit 1

ENTRYPOINT ["/bin/sh", "-c", "/usr/bin/start-jod"]
